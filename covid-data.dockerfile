FROM golang:1.16-alpine3.13 AS builder
RUN apk add --no-cache git

COPY src/covid-data/go.* /go/src/covid-data/

ENV GOPATH=/go
WORKDIR /go/src/covid-data
RUN go mod download

COPY src/covid-data /go/src/covid-data
RUN go build -ldflags "-X main.BuildVersion=`date '+%Y.%m.%d.%H.%M.%S'`" covid-data

FROM alpine:3.13
RUN adduser -S -h /app retreiver
COPY --from=builder /go/src/covid-data/covid-data /app/covid-data
USER retreiver
ENTRYPOINT [ "/app/covid-data" ]
