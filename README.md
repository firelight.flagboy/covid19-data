# Covid19 Data Fr

## Api

the end point give an array of `Report`

- `Report`[]

### Report

| Key                         | Type                    | Required | Description                                 |
| --------------------------- | ----------------------- | -------- | ------------------------------------------- |
| `casConfirmes`              | `uint`                  | `true`   | number of confirmed case                    |
| `casConfirmesEhpad`         | `uint`                  |          | number of confirmed case in retirement home |
| `deces`                     | `uint`                  | `true`   | number of death                             |
| `decesEhpad`                | `uint`                  |          | number of death in retirement home          |
| `reanimation`               | `uint`                  |          | number of people in heavy care              |
| `hospitalises`              | `uint`                  |          | number of people in hospital                |
| `gueris`                    | `uint`                  |          | number of healed people                     |
| `date`                      | `string(date iso-8601)` | `true`   | date of the report                          |
| `code`                      | `string`                | `true`   | country code                                |
| `nom`                       | `string`                | `true`   | country name                                |
| `testsRealises`             | `uint`                  |          | number of test done that day                |
| `testsPositifs`             | `uint`                  |          | number of positive tests                    |
| `testsRealisesDetails`      | `TestDetail[]`          |          | breakdown of tests                          |
| `testsPositifsDetails`      | `TestDetail[]`          |          | breakdown of tests that were positive       |
| `nouvellesHospitalisations` | `uint`                  |          | number of new people in hospital            |
| `nouvellesReanimations`     | `uint`                  |          | number of new people in heavy care          |
| `tauxIncidence`             | `float`                 |          |                                             |
| `tauxReproductionEffectif`  | `float`                 |          | R0                                          |
| `tauxOccupationRea`         | `float`                 |          | rate of heavy care usage (%)                |
| `tauxPositiviteTests`       | `float`                 |          | rate of positivity of test (%)              |

### TestDetail

| Key     | Type              |
| ------- | ----------------- |
| `age`   | `string(uint)`    |
| `sexe`  | `'f' / 'm' / '0'` |
| `value` | `uint`            |

## Source

- [End point](https://dashboard.covid19.data.gouv.fr/data/code-FRA.json)
- [Influxdb - Getting Started with go client](https://www.influxdata.com/blog/getting-started-with-the-influxdb-go-client/)
- [Influxdb - Go client doc](https://pkg.go.dev/github.com/influxdata/influxdb-client-go/v2)
- [Run Grafana Docker image](https://grafana.com/docs/grafana/latest/installation/docker/)
- Docker Image
  - [grafana](https://hub.docker.com/r/grafana/grafana)
  - [influxdb](https://hub.docker.com/_/influxdb)
- [k8s - CronJob](https://kubernetes.io/docs/concepts/workloads/controllers/cron-jobs/)
