package report

import (
	"encoding/json"
	"time"
)

// TestDetail represent a certain category of a test (json raw)
type TestDetail struct {
	Age   string   `json:"age"`
	Sexe  SexeType `json:"sexe"`
	Value uint     `json:"value"`
}

// Identifaction contains all the info for a unique report
type Identifaction struct {
	Date Iso8601Time `json:"date"`
	Code string      `json:"code"`
	Name string      `json:"nom"`
}

type Rate struct {
	Incidence      float32 `json:"tauxIncidence"`
	R0             float32 `json:"tauxReproductionEffectif"`
	HeavyCareUsage float32 `json:"tauxOccupationRea"`
	TestPositivity float32 `json:"tauxPositiviteTests"`
}

type Testing struct {
	Total           uint         `json:"testsRealises"`
	Positive        uint         `json:"testsPositifs"`
	Details         []TestDetail `json:"testsRealisesDetails"`
	PositiveDetails []TestDetail `json:"testsPositifsDetails"`
}

type Data struct {
	ConfirmedCase uint `json:"casConfirmes"`
	Death         uint `json:"deces"`

	ConfirmedCaseEhpad uint `json:"casConfirmesEhpad"`
	DeathEhpad         uint `json:"decesEhpad"`

	HeavyCare       uint `json:"reanimation"`
	Hospitalization uint `json:"hospitalises"`
	Healed          uint `json:"gueris"`

	NewHospitalization uint `json:"nouvellesHospitalisations"`
	NewHeavyCare       uint `json:"nouvellesReanimations"`
}

type Vaccination struct {
	DeliveryModerna uint `json:"livraisonsCumulNombreDosesModerna"`
	DeliveryPfizer  uint `json:"livraisonsCumulNombreDosesPfizer"`
	TotalDelivery   uint `json:"livraisonsCumulNombreTotalDoses"`

	StockEhpadPfizer uint `json:"stockEhpadNombreDosesPfizer"`
	StockModerna     uint `json:"stockNombreDosesModerna"`
	StockPfizer      uint `json:"stockNombreDosesPfizer"`
	TotalStock       uint `json:"stockNombreTotalDoses"`

	FirstInjection    uint `json:"cumulPremieresInjections"`
	NewFirstInjection uint `json:"nouvellesPremieresInjections"`
	AppointmentWeek1  uint `json:"prisesRendezVousSemaineRang1"`
	AppointmentWeek2  uint `json:"prisesRendezVousSemaineRang2"`
	TotalAppointment  uint `json:"totalPrisesRendezVousSemaine"`
}

type reportJSON struct {
	Identifaction
	Rate
	Testing
	Data
	Vaccination
}

// Report represent a report
type Report struct {
	reportJSON
	Date         time.Time
	LocationType LocationType
}

// UnmarshalJSON convert json data to struct
func (r *Report) UnmarshalJSON(data []byte) (err error) {
	var _r reportJSON

	err = json.Unmarshal(data, &_r)
	*r = Report{
		reportJSON:   _r,
		Date:         time.Time(_r.Date),
		LocationType: getLocationType(_r.Code),
	}
	return
}

func getLocationType(code string) LocationType {
	switch code[:3] {
	case "DEP":
		return Department
	case "REG":
		return Region
	case "FRA":
		return Country
	default:
		return Unknown
	}
}
