package report

// SexeType represent a sexe
type SexeType string

const (
	Male   SexeType = "m"
	Female SexeType = "f"
	Other  SexeType = "0"
)

// LocationType the type of the location
type LocationType string

const (
	Department LocationType = "department"
	Region     LocationType = "region"
	Country    LocationType = "country"
	Unknown    LocationType = "unknown"
)
