package report

import (
	"encoding/json"
	"time"
)

// Iso8601Time represent a date in the format yyyy-mm-dd
type Iso8601Time time.Time

func (t *Iso8601Time) UnmarshalJSON(data []byte) error {
	var s string
	if err := json.Unmarshal(data, &s); err != nil {
		return err
	}
	_t, err := time.Parse("2006-01-02", s)
	if err != nil {
		return err
	}
	*t = Iso8601Time(_t)
	return nil
}
