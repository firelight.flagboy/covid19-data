package main

import (
	"covid-data/report"

	log "github.com/sirupsen/logrus"

	influxdb2 "github.com/influxdata/influxdb-client-go/v2"
	influxdbAPI "github.com/influxdata/influxdb-client-go/v2/api"
	influxdbWrite "github.com/influxdata/influxdb-client-go/v2/api/write"
)

func sendReport(writer influxdbAPI.WriteAPI, report *report.Report) {
	reportPoint := getReportPoint(report)
	reportRatePoint := getReportRate(report)

	writer.WritePoint(reportPoint)
	writer.WritePoint(reportRatePoint)
}

func sendReports(writer influxdbAPI.WriteAPI, reports []report.Report) {
	for idx, report := range reports {
		l := log.WithFields(log.Fields{
			"current": idx + 1,
			"total":   len(reports),
		})
		if len(report.Name) > 0 {
			l.Debug("sending report")
			sendReport(writer, &report)
		} else {
			l.WithFields(log.Fields{
				"report": report,
				"reason": "missing name",
			}).Debug("ignore report")
		}
	}
}

func getReportPoint(report *report.Report) *influxdbWrite.Point {
	return influxdb2.NewPoint("report",
		map[string]string{
			"location":      report.Name,
			"location_code": report.Code,
			"location_type": string(report.LocationType),
			"unit":          "human",
		},
		map[string]interface{}{
			"case":   int(report.ConfirmedCase),
			"death":  int(report.Death),
			"healed": int(report.Healed),

			"heavy_care":       int(report.HeavyCare),
			"heavy_care_entry": int(report.NewHeavyCare),

			"hospitalization":       int(report.Hospitalization),
			"hospitalization_entry": int(report.NewHospitalization),

			"test_total":    int(report.Testing.Total),
			"test_positive": int(report.Testing.Positive),
		},
		report.Date)
}

func getReportRate(report *report.Report) *influxdbWrite.Point {
	return influxdb2.NewPoint("report-rate",
		map[string]string{
			"location":      report.Name,
			"location_code": report.Code,
			"location_type": string(report.LocationType),
			"unit":          "percentage",
		},
		map[string]interface{}{
			"r0":              report.Rate.R0,
			"incidence":       report.Rate.Incidence,
			"heavy_usage":     report.Rate.HeavyCareUsage,
			"test_positivity": report.Rate.TestPositivity,
		},
		report.Date)
}
