package main

import (
	"flag"
	"os"
)

type option struct {
	APIURLRoot string
	DBHost     string
	DBToken    string
	DBUser     string
	DBPass     string
	DBOrg      string
	DBName     string
}

func newOption() *option {
	return &option{
		APIURLRoot: "https://dashboard.covid19.data.gouv.fr/data",
		DBHost:     "http://influxdb:8086",
		DBToken:    "",
		DBUser:     "",
		DBOrg:      "covid-data",
		DBName:     "covid-data-report",
	}
}

func setUsingEnv(key string, value *string) {
	if v, exist := os.LookupEnv(key); exist == true {
		*value = v
	}
}

func (opt *option) RetrieveFromEnv() {
	setUsingEnv("INFLUXDB_USER", &opt.DBUser)
	setUsingEnv("INFLUXDB_PASSWORD", &opt.DBPass)
	setUsingEnv("INFLUXDB_TOKEN", &opt.DBToken)
	setUsingEnv("INFLUXDB_HOST", &opt.DBHost)
	setUsingEnv("INFLUXDB_DB", &opt.DBName)
	setUsingEnv("API_URL", &opt.APIURLRoot)
}

func (opt *option) RetrieveFromCli() {
	flag.StringVar(&opt.APIURLRoot, "api-url", opt.APIURLRoot, "set api endpoint url")
	flag.StringVar(&opt.DBHost, "influxdb-host", opt.DBHost, "set influxdb host url")
	flag.StringVar(&opt.DBToken, "influxdb-token", opt.DBToken, "set influxdb auth token (can be empty)")
	flag.StringVar(&opt.DBUser, "influxdb-user", opt.DBUser, "set influxdb user")
	flag.StringVar(&opt.DBName, "influxdb-name", opt.DBName, "set database to use")

	flag.Parse()
}

func (opt *option) RetrieveSettings() {
	opt.RetrieveFromEnv()
	opt.RetrieveFromCli()
}
