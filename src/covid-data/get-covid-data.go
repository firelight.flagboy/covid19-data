package main

import (
	"covid-data/report"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"time"

	log "github.com/sirupsen/logrus"

	influxdb2 "github.com/influxdata/influxdb-client-go/v2"
	"github.com/influxdata/influxdb-client-go/v2/api"
	influxdbLog "github.com/influxdata/influxdb-client-go/v2/log"
)

const (
	FranceGlobalReport = "code-FRA.json"
	DateReportTimeFMT  = "date-2006-01-02.json"
)

var BuildVersion = "Development"

func getDataReaderFromURL(url string) io.ReadCloser {
	log.WithField("url", url).Info("retrieve data")
	resp, err := http.Get(url)
	if err != nil {
		log.WithFields(log.Fields{
			"url":   url,
			"error": err,
		}).Fatal("request failed")
	}
	if resp.StatusCode != http.StatusOK {
		log.WithFields(log.Fields{
			"url":         url,
			"status_code": resp.StatusCode,
		}).Fatal("bad status code")
	}
	return resp.Body
}

func readRawData(reader io.ReadCloser) []byte {
	rawData, err := ioutil.ReadAll(reader)
	if err != nil {
		log.WithField("error", err).Fatalf("failed to read data from reader")
	}
	return rawData
}

func getReportsFromByte(rawData []byte) []report.Report {
	var reports []report.Report
	if err := json.Unmarshal(rawData, &reports); err != nil {
		log.WithField("error", err).Warn("failed to parse data as json")
	}
	return reports
}

func main() {
	opt := newOption()
	opt.RetrieveSettings()
	log.SetLevel(log.InfoLevel)

	log.WithField("version", BuildVersion).Info("current version")
	client, writeAPI := configureInfluxdbConn(opt.DBHost, opt.DBUser, opt.DBPass, opt.DBOrg, opt.DBName)
	defer client.Close()

	reports := getReportFromGlobal(opt.APIURLRoot)

	pool := make(chan *time.Time, len(reports))

	for idx := range reports {
		pool <- &reports[idx].Date
	}

	sendReports(writeAPI, reports)
DateReportLoop:
	for {
		select {
		case date := <-pool:
			log.WithField("date", date).Trace("got date")
			reports := getReportFromDate(opt.APIURLRoot, date)
			sendReports(writeAPI, reports)
		default:
			break DateReportLoop
		}
	}
	close(pool)

	writeAPI.Flush()
	if gotError := enumerateErrors(writeAPI.Errors()); gotError {
		log.Fatal("got the above errors")
	} else {
		log.Info("done sending report")
	}
}

func configureInfluxdbConn(dbhost, dbuser, dbpass, dborg, dbname string) (influxdb2.Client, api.WriteAPI) {
	influxdbOption := influxdb2.DefaultOptions()
	influxdbOption.SetLogLevel(influxdbLog.WarningLevel)
	client := influxdb2.NewClientWithOptions(dbhost, fmt.Sprintf("%s:%s", dbuser, dbpass), influxdbOption)
	log.WithFields(log.Fields{
		"influxdb_host": dbhost,
	}).Info("configured db host")

	log.WithFields(log.Fields{
		"org":    dborg,
		"dbname": dbname,
	}).Info("setting database config")
	writeAPI := client.WriteAPI(dborg, dbname)
	return client, writeAPI
}

func getReportFromGlobal(apiRoot string) []report.Report {
	return getReportFromFile(apiRoot, FranceGlobalReport)
}

func getReportFromDate(apiRoot string, date *time.Time) []report.Report {
	return getReportFromFile(apiRoot, date.Format(DateReportTimeFMT))
}

func getReportFromFile(apiRoot, file string) []report.Report {
	data := getDataReaderFromURL(fmt.Sprintf("%s/%s", apiRoot, file))
	jsonByte := readRawData(data)
	data.Close()
	log.WithFields(log.Fields{
		"byte_size": len(jsonByte),
	}).Info("read all data")
	reports := getReportsFromByte(jsonByte)
	log.WithFields(log.Fields{
		"report_count": len(reports),
	}).Info("report parsed")
	return reports
}

func enumerateErrors(errors <-chan error) bool {
	gotError := false
ErrLoop:
	for {
		select {
		case err := <-errors:
			log.WithField("error", err).Errorf("got error")
			gotError = true
		default:
			break ErrLoop
		}
	}
	return gotError
}
