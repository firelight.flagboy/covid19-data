FROM grafana/grafana:7.3.7

WORKDIR /etc/grafana/provisioning

COPY providers/* dashboards/
COPY datasources/* datasources/
COPY dashboards/* /etc/dashboards/
