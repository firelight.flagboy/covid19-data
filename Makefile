all: covid-data/scrapper covid-data/grafana

covid-data/scrapper: covid-data.dockerfile src/*
	docker build -t $@:latest -f covid-data.dockerfile .

covid-data/grafana: grafana/*
	docker build -t $@:latest -f grafana/grafana.dockerfile grafana

.PHONY: all covid-data/scrapper covid-data/grafana
